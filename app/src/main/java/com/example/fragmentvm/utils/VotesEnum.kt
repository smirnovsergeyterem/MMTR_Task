package com.example.fragmentvm.utils

enum class VotesEnum(val value: Int = -1) {
    UNDEFINED(-1),
    DOWN(0),
    UP(1)
}