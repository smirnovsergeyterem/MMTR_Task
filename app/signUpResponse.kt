data class signUpResponse(
    val level: String,
    val message: String,
    val status: Int
)